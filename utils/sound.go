package utils

import (
	"github.com/hajimehoshi/go-mp3"
	"github.com/hajimehoshi/oto/v2"
	"log"
	"os"
	"time"
)

func PlaySound(path string) {
	file, err := os.Open(path)
	if err != nil {
		log.Printf("opening my-file.mp3 failed: " + err.Error())
	}

	// Decode file. This process is done as the file plays so it won't
	// load the whole thing into memory.
	decodedMp3, err := mp3.NewDecoder(file)
	if err != nil {
		log.Printf("mp3.NewDecoder failed: " + err.Error())
	}
	otoCtx, readyChan, err := oto.NewContext(86100, 1, 2)
	if err != nil {
		log.Printf("oto.NewContext failed: " + err.Error())
	}
	// It might take a bit for the hardware audio devices to be ready, so we wait on the channel.
	<-readyChan
	player := otoCtx.NewPlayer(decodedMp3)

	// Play starts playing the sound and returns without waiting for it (Play() is async).
	player.Play()

	// We can wait for the sound to finish playing using something like this
	for player.IsPlaying() {
		time.Sleep(time.Millisecond)
	}
	// Close file only after you finish playing
	err = file.Close()
	if err != nil {
		log.Printf("close sound failed" + err.Error())
	}

}
