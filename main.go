package main

import (
	"context"
	"errors"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/micmonay/keybd_event"
	"github.com/rakyll/openai-go"
	"github.com/rakyll/openai-go/audio"
	"github.com/rakyll/openai-go/chat"
	"golang.design/x/clipboard"
	"golang.design/x/hotkey"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"time"
	"whisper_bot/utils"
)

func main() {
	telegramToken := os.Getenv("TELEGRAM_API_TOKEN")
	openaiToken := os.Getenv("OPENAI_TOKEN")

	bot, err := tgbotapi.NewBotAPI(telegramToken)
	if err != nil {
		panic(err)
	}
	bot.Debug = false

	// Create a new UpdateConfig struct with an offset of 0. Offsets are used
	// to make sure Telegram knows we've handled previous values, and we don't need them repeated.
	updateConfig := tgbotapi.NewUpdate(0)
	session := openai.NewSession(openaiToken)
	whisperClient := audio.NewClient(session, "")
	chatClient := chat.NewClient(session, "")

	go localRecording(whisperClient, chatClient)
	// Tell Telegram we should wait up to 30 seconds on each request for an
	// update. This way we can get information just as quickly as making many
	// frequent requests without having to send nearly as many.
	updateConfig.Timeout = 30
	updates := bot.GetUpdatesChan(updateConfig)

	log.Printf("started\n")

	for update := range updates {
		message := update.Message
		if message != nil {
			voice := message.Voice
			if voice != nil {
				file, err := bot.GetFile(tgbotapi.FileConfig{FileID: voice.FileID})
				if err != nil {
					log.Printf("Err %s", err)
				}

				out, err := os.CreateTemp("", "*.oga")
				if err != nil {
					log.Printf("Err create voice file %s", err)
				}
				sourceOgaUrl := file.Link(telegramToken)
				resp, err := http.Get(sourceOgaUrl)
				_, err = io.Copy(out, resp.Body)
				if err != nil {
					log.Printf("Err copy loaded voice to disk %s", err)
				}
				err = resp.Body.Close()
				if err != nil {
					log.Printf("Err close voice load %s", err)
				}
				err = out.Close()
				if err != nil {
					log.Printf("Err close file %s", err)
				}

				mp3FilePath := out.Name() + ".mp3"

				ffmpegPath, err := exec.LookPath("ffmpeg")
				if err != nil {
					log.Printf("ffmpeg not found %s", err)
				}

				cmd := exec.Command(ffmpegPath, "-i", out.Name(), mp3FilePath)
				if errors.Is(cmd.Err, exec.ErrDot) {
					cmd.Err = nil
				}
				if err := cmd.Run(); err != nil {
					log.Printf("Err ffmpeg %s", err)
				}

				mp3File, err := os.Open(mp3FilePath)
				if err != nil {
					log.Fatalf("error opening audio file: %v", err)
				}

				whisperResponse, err := whisperClient.CreateTranscription(context.TODO(), &audio.CreateTranscriptionParams{
					Language:    "ru",
					Audio:       mp3File,
					AudioFormat: "mp3",
				})
				if err != nil {
					log.Fatalf("error transcribing file: %v", err)
				} else {
					messageConfig := tgbotapi.NewMessage(message.Chat.ID, whisperResponse.Text)
					_, err = bot.Send(messageConfig)
					if err != nil {
						log.Fatalf("error closing mp3 file: %v", err)
					}
				}

				err = mp3File.Close()
				if err != nil {
					log.Fatalf("error closing mp3 file: %v", err)
				}
				err = os.Remove(out.Name())
				if err != nil {
					log.Printf("Err remove tmp file %s", err)
				}

				err = os.Remove(mp3FilePath)
				if err != nil {
					log.Printf("Err remove mp3 file %s", err)
				}
			}
		} else if update.CallbackQuery != nil {
			// Respond to the callback query, telling Telegram to show the user a message with the data received.
			callback := tgbotapi.NewCallback(update.CallbackQuery.ID, update.CallbackQuery.Data)
			if _, err := bot.Request(callback); err != nil {
				log.Printf("Unable to answer callback %s", err)
				continue
			}

		}
	}
}

func doNotChange(input string, _ *chat.Client) (string, error) {
	return input, nil
}

func translateRuToEng(input string, chatClient *chat.Client) (string, error) {
	resp, err := chatClient.CreateCompletion(context.Background(), &chat.CreateCompletionParams{
		Messages: []*chat.Message{
			{Role: "user", Content: fmt.Sprintf("translate from russian to english: %s", input)},
		},
	})
	if err != nil {
		return "", err
	} else {
		for _, choice := range resp.Choices {
			msg := choice.Message
			return msg.Content, nil
		}
	}
	return "empty response", nil
}

func chatGptAnswer(input string, chatClient *chat.Client) (string, error) {
	resp, err := chatClient.CreateCompletion(context.Background(), &chat.CreateCompletionParams{
		Messages: []*chat.Message{
			{Role: "user", Content: input},
		},
	})
	if err != nil {
		return "", err
	} else {
		for _, choice := range resp.Choices {
			msg := choice.Message
			return msg.Content, nil
		}
	}
	return "empty response", nil
}

func localRecording(whisperClient *audio.Client, chatClient *chat.Client) {
	go listenVoiceByHotkey(whisperClient, chatClient, hotkey.New([]hotkey.Modifier{hotkey.ModCtrl}, hotkey.Key2), doNotChange)
	go listenVoiceByHotkey(whisperClient, chatClient, hotkey.New([]hotkey.Modifier{hotkey.ModCtrl}, hotkey.Key3), translateRuToEng)
	go listenVoiceByHotkey(whisperClient, chatClient, hotkey.New([]hotkey.Modifier{hotkey.ModCtrl}, hotkey.Key4), chatGptAnswer)
}

func listenVoiceByHotkey(whisperClient *audio.Client, chatClient *chat.Client, recordHk *hotkey.Hotkey, textConverter func(string, *chat.Client) (string, error)) {

	err := recordHk.Register()
	if err != nil {
		log.Fatalf("hotkey: failed to register hotkey: %v", err)
	}
	defer func(recordHk *hotkey.Hotkey) {
		err := recordHk.Unregister()
		if err != nil {
			log.Printf("hotkey: %v cannot be unregistered\n", recordHk)
		}
	}(recordHk)

	pasteKeyBind, err := keybd_event.NewKeyBonding()
	if err != nil {
		panic(err)
	}

	// Select keys to be pressed
	pasteKeyBind.SetKeys(keybd_event.VK_V)
	pasteKeyBind.HasCTRL(true)

	defer func(kb *keybd_event.KeyBonding) {
		err := kb.Release()
		if err != nil {
			log.Printf("keybind: cannot be released\n")
		}
	}(&pasteKeyBind)

	for {
		<-recordHk.Keydown()
		text, err := getText(recordHk, whisperClient)
		if err != nil {
			log.Printf("Err %s", err)
			continue
		}
		text, err = textConverter(text, chatClient)
		if err != nil {
			log.Printf("Err %s", err)
			continue
		}
		clipboard.Write(clipboard.FmtText, []byte(text))
		err = pasteKeyBind.Press()
		if err != nil {
			log.Printf("Err press ctrl v %s", err)
		}
		time.Sleep(10 * time.Millisecond)
		err = pasteKeyBind.Release()
		if err != nil {
			log.Printf("Err release ctrl v %s", err)
		}

	}
}

func getText(recordHk *hotkey.Hotkey, whisperClient *audio.Client) (string, error) {
	utils.PlaySound("./start.mp3")

	recordWav, err := os.CreateTemp("", "*.wav")
	if err != nil {
		log.Printf("Err create voice file %s", err)
		return "", err
	}

	// https://sourceforge.net/projects/sox/
	soxPath, err := exec.LookPath("sox")
	if err != nil {
		log.Printf("sox not found %s", err)
		return "", err
	}

	cmd := exec.Command(soxPath, "-t", "waveaudio", "-d", recordWav.Name())
	err = cmd.Start()
	if err != nil {
		log.Printf("Err starting osx %s", err)
		return "", err
	}
	<-recordHk.Keydown()
	utils.PlaySound("./stop.mp3")
	err = cmd.Process.Kill()
	if err != nil {
		log.Printf("Err stopping osx %s", err)
		return "", err
	}

	err = recordWav.Close()
	if err != nil {
		log.Printf("Err closing record wav %s", err)
		return "", err
	}

	ffmpegPath, err := exec.LookPath("ffmpeg")
	if err != nil {
		log.Printf("ffmpeg not found %s", err)
		return "", err
	}

	mp3FilePath := recordWav.Name() + ".mp3"
	cmd = exec.Command(ffmpegPath, "-i", recordWav.Name(), mp3FilePath)
	if errors.Is(cmd.Err, exec.ErrDot) {
		cmd.Err = nil
	}
	if err := cmd.Run(); err != nil {
		log.Printf("Err ffmpeg %s", err)
		return "", err
	}

	mp3File, err := os.Open(mp3FilePath)
	if err != nil {
		log.Fatalf("error opening audio file: %v", err)
		return "", err
	}

	whisperResponse, err := whisperClient.CreateTranscription(context.TODO(), &audio.CreateTranscriptionParams{
		Language:    "ru",
		Audio:       mp3File,
		AudioFormat: "mp3",
	})
	if err != nil {
		log.Printf("error transcribing file: %v", err)
		return "", err
	}

	err = mp3File.Close()
	if err != nil {
		log.Printf("Err closing record mp3 %s", err)
	}
	err = os.Remove(recordWav.Name())
	if err != nil {
		log.Printf("Err remove tmp file %s", err)
	}

	err = os.Remove(mp3FilePath)
	if err != nil {
		log.Printf("Err remove mp3 file %s", err)
	}
	return whisperResponse.Text, nil
}
