module whisper_bot

go 1.20

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/hajimehoshi/go-mp3 v0.3.4
	github.com/hajimehoshi/oto/v2 v2.4.0
	github.com/micmonay/keybd_event v1.1.1
	github.com/rakyll/openai-go v1.0.9
	golang.design/x/clipboard v0.7.0
	golang.design/x/hotkey v0.4.1
)

require (
	github.com/ebitengine/purego v0.3.0 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/image v0.6.0 // indirect
	golang.org/x/mobile v0.0.0-20230301163155-e0f57694e12c // indirect
	golang.org/x/sys v0.5.0 // indirect
)
